flask-video-streaming
=====================

Based on supporting code for the article [video streaming with Flask](http://blog.miguelgrinberg.com/post/video-streaming-with-flask) and its follow-up [Flask Video Streaming Revisited](http://blog.miguelgrinberg.com/post/flask-video-streaming-revisited).

## Superseded by https://gitlab.com/openflexure/openflexure-microscope-server
